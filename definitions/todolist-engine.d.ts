declare module 'todolist-engine' {
  import { Dispatch, Store } from 'redux';
  import { VNode } from 'snabbdom/vnode';

  type Component<T> = (props: T) => VNode;

  interface Main<T, U> {
    store: Store<T>;
    view: Component<T & Dispatch<U>>;
  }
}
