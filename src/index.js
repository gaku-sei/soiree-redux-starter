import { init } from "snabbdom";
import { h } from "snabbdom/h";
import styleModule from "snabbdom/modules/style";

const patch = init([styleModule]);

patch(
  document.getElementById("root"),
  h(
    "h1",
    {
      style: {
        display: "flex",
        width: "100%",
        height: "90%",
        alignItems: "center",
        justifyContent: "center",
        fontSize: "50px",
      },
    },
    "Hello Redux!",
  ),
);

// import { createStore } from "redux";

// import run from "../lib";
// import App from "../lib/components/App";

// const createNewTodo = (id, value) => ({
//   completed: false,
//   id,
//   value,
// });

// const view = App({
//   // Every functions are action creators
//   // onChangeNewTodoValue: setNewTodoValue,
//   // onCheckCompletenessInput: setTodoCompleteness,
//   // onClickCompleteAll: completeEveryTodos,
//   // onClickDeleteCompletedTodos: deleteCompletedTodos,
//   // onDeleteTodo: deleteTodo,
//   // onSelectFilter: setFilter,
//   // onSubmitNewTodo: addNewTodo,
// });

// const reducer = (state, action) => {
//   return state;
// };

// // Initial State
// const initialState = {
//   counter: 0,
//   filter: "all",
//   newTodoValue: "",
//   todos: [],
// };

// // Store
// const store = createStore(reducer, initialState);

// // Boilerplate
// run("root", { store, view });
