import run from '../lib';
import App from '../lib/components/App';

import { bindActionCreate, combineReducers, createStore } from 'redux';
import * as R from 'ramda';

// Actions
const ADD_NEW_TODO = 'ADD_NEW_TODO';
const COMPLETE_EVERY_TODOS = 'COMPLETE_EVERY_TODOS';
const DELETE_COMPLETED_TODOS = 'DELETE_COMPLETED_TODOS';
const DELETE_TODO = 'DELETE_TODO';
const SET_FILTER = 'SET_FILTER';
const SET_NEW_TODO_VALUE = 'SET_NEW_TODO_VALUE';
const SET_TODO_COMPLETENESS = 'SET_TODO_COMPLETENESS';

// Action Creators
const addNewTodo = value => ({
  type: ADD_NEW_TODO,
  value,
});

const completeEveryTodos = () => ({
  type: COMPLETE_EVERY_TODOS,
});

const deleteCompletedTodos = () => ({
  type: DELETE_COMPLETED_TODOS,
});

const deleteTodo = id => ({
  type: DELETE_TODO,
  id,
});

const setFilter = filter => ({
  type: SET_FILTER,
  filter,
});

const setNewTodoValue = value => ({
  type: SET_NEW_TODO_VALUE,
  value,
});

const setTodoCompleteness = (id, completed) => ({
  type: SET_TODO_COMPLETENESS,
  completed,
  id,
});

// Reducer
const reducer = action => {
  const {
    always, any, apply, complement, compose, concat, either, evolve, equals,
    filter, findIndex, flip, identity, ifElse, lensProp, map, memoize, of,
    over, propEq, reduce, remove, repeat, set, uncurryN, view,
  } = R;

  const completedLens = lensProp('completed');
  const filterLens = lensProp('filter');
  const newTodoValueLens = lensProp('newTodoValue');
  const todosLens = lensProp('todos');
  const todosView = memoize(view(todosLens));
  const newTodoValueView = memoize(view(newTodoValueLens));
  const anyValueEq = compose(any, propEq('value'));

  const createTodo = value => ({ completed: false, id: Math.round(Math.random() * 100000), value });

  switch (action.type) {
    case ADD_NEW_TODO:
      return ifElse(
        either(compose(equals(''), newTodoValueView), compose(anyValueEq(action.value), todosView)),
        identity,
        evolve({
          newTodoValue: always(''),
          todos: concat(of(createTodo(action.value))),
        }),
      );

    case COMPLETE_EVERY_TODOS:
      return over(todosLens, map(set(lensProp('completed'), true)));

    case DELETE_COMPLETED_TODOS:
      return over(todosLens, filter(propEq('completed', false)));

    // TOREFACT
    case DELETE_TODO:
      return over(todosLens,
        compose(
          apply(uncurryN(2, compose(flip(remove)(1), memoize(findIndex(propEq('id', action.id)))))),
          flip(repeat)(2),
        ),
      );

    case SET_FILTER:
      return set(filterLens, action.filter);

    case SET_NEW_TODO_VALUE:
      return set(newTodoValueLens, action.value);

    case SET_TODO_COMPLETENESS:
      return over(todosLens, reduce(uncurryN(2, acc =>
        ifElse(
          complement(propEq)('id', action.id),
          compose(concat(acc), of),
          compose(concat(acc), compose(of, set(completedLens, action.completed))),
        )), []));

    default: return identity;
  }
};

// View
const view = App({
  onChangeNewTodoValue: setNewTodoValue,
  onCheckCompletenessInput: setTodoCompleteness,
  onClickCompleteAll: completeEveryTodos,
  onClickDeleteCompletedTodos: deleteCompletedTodos,
  onDeleteTodo: deleteTodo,
  onSelectFilter: setFilter,
  onSubmitNewTodo: addNewTodo,
});

// Initial State
const initialState = {
  filter: 'all',
  newTodoValue: '',
  todos: [],
};

// Store
const store = createStore((state, action) => reducer(action)(state), initialState);

// Boilerplate
run('root', { store, view });
