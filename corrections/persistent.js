import run from '../lib';
import App from '../lib/components/App';

import { applyMiddleware, bindActionCreate, combineReducers, createStore } from 'redux';
import thunk from 'redux-thunk';

// Actions
const ADD_NEW_TODO = 'ADD_NEW_TODO';
const COMPLETE_EVERY_TODOS = 'COMPLETE_EVERY_TODOS';
const DELETE_COMPLETED_TODOS = 'DELETE_COMPLETED_TODOS';
const DELETE_TODO = 'DELETE_TODO';
const SET_FILTER = 'SET_FILTER';
const SET_NEW_TODO_VALUE = 'SET_NEW_TODO_VALUE';
const SET_TODO_COMPLETENESS = 'SET_TODO_COMPLETENESS';

// Action Creators
const addNewTodo = value => async (dispatch, getState) => {
  if (value === '') {
    return;
  }

  if (getState().todos.some(todo => todo.value === value)) {
    return;
  }

  const response = await fetch('/todos', {
    body: JSON.stringify({ completed: false, value }),
    headers: {
      'Content-Type': 'application/json',
    },
    method: 'POST',
  });

  const todo = await response.json();

  dispatch({
    type: ADD_NEW_TODO,
    todo,
  });
};

const completeEveryTodos = () => async (dispatch, getState) => {
  await Promise.all(getState().todos.map(({ id }) =>
    fetch(`/todos/${id}`, {
      body: JSON.stringify({ completed: true }),
      headers: {
        'Content-Type': 'application/json',
      },
      method: 'PATCH',
    })));

  dispatch({
    type: COMPLETE_EVERY_TODOS,
  });
};

const deleteCompletedTodos = () => async (dispatch, getState) => {
  await Promise.all(getState().todos.filter(({ completed }) => completed).map(({ id }) =>
    fetch(`/todos/${id}`, {
      method: 'DELETE',
    })));

  dispatch({
    type: DELETE_COMPLETED_TODOS,
  });
};

const deleteTodo = id => async (dispatch) => {
  await fetch(`/todos/${id}`, {
    method: 'DELETE',
  });

  dispatch({
    type: DELETE_TODO,
    id,
  });
};

const setFilter = filter => ({
  type: SET_FILTER,
  filter,
});

const setNewTodoValue = value => ({
  type: SET_NEW_TODO_VALUE,
  value,
});

const setTodoCompleteness = (id, completed) => async dispatch => {
  await fetch(`/todos/${id}`, {
    body: JSON.stringify({ completed }),
    headers: {
      'Content-Type': 'application/json',
    },
    method: 'PATCH',
  });

  dispatch({
    type: SET_TODO_COMPLETENESS,
    completed,
    id,
  });
};

// Reducer
const reducer = (state, action) => {
  switch (action.type) {
    case ADD_NEW_TODO: {
      const { todo } = action;
      const { todos } = state;

      return {
        ...state,
        newTodoValue: '',
        todos: todos.concat(todo),
      };
    }

    case COMPLETE_EVERY_TODOS: {
      const { todos } = state;

      return {
        ...state,
        todos: todos.map(todo => ({ ...todo, completed: true })),
      };
    }

    case DELETE_COMPLETED_TODOS: {
      const { todos } = state;

      return {
        ...state,
        todos: todos.filter(({ completed }) => !completed),
      };
    }

    case DELETE_TODO: {
      const { id } = action;
      const { todos } = state;

      const index = todos.findIndex(todo => todo.id === id);

      return {
        ...state,
        todos: todos.slice(0, index).concat(todos.slice(index + 1, todos.length)),
      };
    }

    case SET_FILTER: {
      const { filter } = action;

      return {
        ...state,
        filter,
      };
    }

    case SET_NEW_TODO_VALUE: {
      const { value } = action;

      return {
        ...state,
        newTodoValue: value,
      };
    }

    case SET_TODO_COMPLETENESS: {
      const { completed, id } = action;
      const { todos } = state;

      return {
        ...state,
        todos: todos.reduce((acc, todo) => {
          if (todo.id !== id) {
            return acc.concat(todo);
          }

          return acc.concat({ ...todo, completed });
        }, []),
      };
    }

    default: return state;
  }
};

// View
const view = App({
  onChangeNewTodoValue: setNewTodoValue,
  onCheckCompletenessInput: setTodoCompleteness,
  onClickCompleteAll: completeEveryTodos,
  onClickDeleteCompletedTodos: deleteCompletedTodos,
  onDeleteTodo: deleteTodo,
  onSelectFilter: setFilter,
  onSubmitNewTodo: addNewTodo,
});

(async () => {
  const response = await fetch('/todos');
  const todos = await response.json();

  // Initial State
  const initialState = {
    filter: 'all',
    newTodoValue: '',
    todos,
  }

  // Store
  const store = createStore(reducer, initialState, applyMiddleware(thunk));

  // Boilerplate
  run('root', { store, view });
})();
