const createStore = (reducer, initialState) => {
  const subscribers = [];
  let internalState = initialState;

  return {
    dispatch(action) {
      internalState = reducer(internalState, action);
      subscribers.forEach(subscriber => subscriber());
    },

    getState() {
      return internalState;
    },

    subscribe(listener) {
      subscribers.push(listener);
    },
  };
};

const reducer = (state, action) => {
  switch (action.type) {
    case 'SET':
      return { name: action.name };
    case 'RESET':
      return { name: '' };
    default:
      return state;
  }
};

const store = createStore(reducer, { name: '' });

store.subscribe(() => {
  console.log(store.getState());
});

store.dispatch({ type: 'SET', name: 'Toto' });
store.dispatch({ type: 'RESET' });
