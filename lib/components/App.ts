import { bindActionCreators, Dispatch } from 'redux';
import { h } from 'snabbdom/h';
import { VNode } from 'snabbdom/vnode';

interface Props<T> {
  dispatch: Dispatch<T>;
  filter: 'all' | 'todo' | 'completed';
  newTodoValue: string;
  todos: { completed: boolean; id: string, value: string }[];
}

const update = ({ target }: Event) => ({
  type: 'SET', value: (target as HTMLInputElement).value
});

export default ({
  onChangeNewTodoValue,
  onCheckCompletenessInput,
  onClickCompleteAll,
  onClickDeleteCompletedTodos,
  onDeleteTodo,
  onSelectFilter,
  onSubmitNewTodo,
}: any) => {
  return <T>({ dispatch, filter, newTodoValue, todos }: Props<T>): VNode => (
    h('section.todoapp', [
      h('header.header', [
        h('h1', 'A Faire'),
        h('input.new-todo', {
          props: { type: 'text', placeholder: 'Que devez-vous faire?', autofocus: true, value: newTodoValue },
          on: {
            input(event: KeyboardEvent) {
              dispatch(onChangeNewTodoValue((event.target as HTMLInputElement).value));
            },

            keypress(event: KeyboardEvent) {
              if (event.key === 'Enter') {
                dispatch(onSubmitNewTodo((event.target as HTMLInputElement).value));
              }
            },
          },
        } as any),
      ]),
      ...(!todos.length ? [] : [
        h('section.main', [
          h('input#toggle-all.toggle-all', {
            props: { type: 'checkbox' },
          }),
          h('label', {
            props: { for: 'toggle-all' },
            on: {
              click() {
                dispatch(onClickCompleteAll());
              },
            },
          }, 'Mark all as complete'),
          h('ul.todo-list', todos
            .filter(({ completed }) => filter === 'all' ? true : filter === 'completed' ? completed : !completed)
            .map(({ completed, id, value }) => (
              h('li', {
                class: { completed },
              }, [
                h('div.view', [
                  h('input.toggle', {
                    props: { checked: completed, type: 'checkbox' },
                    on: {
                      change(event: MouseEvent) {
                        dispatch(onCheckCompletenessInput(id, (event.target as HTMLInputElement).checked));
                      },
                    },
                  } as any),
                  h('label', value),
                  h('button.destroy', {
                    on: {
                      click() {
                        dispatch(onDeleteTodo(id));
                      }
                    },
                  }),
                ]),
              ])
           )),
          ),
        ]),
        h('section.footer', [
          h('span.todo-count', [
            h('strong', todos.filter(({ completed }) => !completed).length.toString()),
            ' tâche(s) restante(s)',
          ]),
          h('ul.filters', [
            h('li',
              h('a', {
                class: { selected: filter === 'all' },
                on: {
                  click() {
                    dispatch(onSelectFilter('all'));
                  },
                },
              }, 'Tous')),
            h('li',
              h('a', {
                class: { selected: filter === 'todo' },
                on: {
                  click() {
                    dispatch(onSelectFilter('todo'));
                  },
                },
              }, 'A Faire')),
            h('li',
              h('a', {
                class: { selected: filter === 'completed' },
                on: {
                  click() {
                    dispatch(onSelectFilter('completed'));
                  },
                },
              }, 'Faits')),
          ]),
          !todos.some(({ completed }) => completed) ? null : (
            h('button.clear-completed', {
              on: {
                click() {
                  dispatch(onClickDeleteCompletedTodos());
                }
              },
            }, 'Supprimer les faits')
          ),
        ])
      ]),
    ])
  );
};
