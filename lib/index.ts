import { Action } from 'redux';
import * as snabbdom from 'snabbdom';
import classModule from 'snabbdom/modules/class';
import propsModule from 'snabbdom/modules/props';
import styleModule from 'snabbdom/modules/style';
import eventlistenersModule from 'snabbdom/modules/eventlisteners';
import { Main } from 'todolist-engine';

export default <T extends object, U>(rootSelector: string, { view, store, }: Main<T, U>) => {
  const patch = snabbdom.init([classModule, propsModule, styleModule, eventlistenersModule]);

  let root: any = document.getElementById(rootSelector);

  if (!root) {
    throw new Error('No root div found');
  }

  store.subscribe(() => {
    root = patch(root, view({ ...store.getState() as any, dispatch: store.dispatch }));
  });

  root = patch(root, view({
    ...store.getState() as any,
    dispatch: store.dispatch,
  }));
}
