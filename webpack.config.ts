import * as webpack from 'webpack';
import * as path from 'path';

export default {
  entry: [
    'todomvc-app-css/index.css',
    'babel-polyfill',
    path.resolve(__dirname, 'src'),
  ],
  output: {
    filename: 'bundle.js',
    path: path.resolve(__dirname, 'dist'),
  },
  devServer: {
    proxy: {
      '/todos': {
        target: 'http://localhost:8000',
        secure: false
      },
    },
    publicPath: '/dist/',
  },
  resolve: {
    extensions: ['.js', '.ts'],
  },
  module: {
    rules: [
      {
        test: /\.css$/,
        loaders: ['style-loader', 'css-loader'],
      },
      {
        exclude: /node_modules/,
        test: /\.js$/,
        use: 'babel-loader',
      },
      {
        exclude: /node_modules/,
        test: /\.ts$/,
        use: 'ts-loader',
      },
    ],
  },
} as webpack.Configuration;
